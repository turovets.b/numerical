#include "stdafx.h"
#include "Solver.h"

bool Solving(double** mas, int n, double* Solution, double* f)
{
	//k-����� ����,i-����� ���������� ������ �� i-�� ����
	double t = 0;//����������, � ������� ���� ������� �� i-�� ������ k-��
	double tmp = 0;//�����. ����������
	int* Change = 0;//������ ������������
	double Max; int I, J;//���������� ��� ������ �������� ��������
	Change = new int[n];
	if (!Change)
	{
		cout << "Not enought memory" << endl;
		return false;

	}

	for (int i = 0; i<n; i++)
		Change[i] = i;

	//������ ���
	for (int k = 0; k<n; k++)
	{
		//���� ������� ������� � ���������� "�����" �������
		Max = fabs(mas[k][k]); I = k; J = k;
		for (int i = k; i<n; i++)
		{
			for (int j = k; j<n; j++)
			{
				if (fabs((double)mas[i][j])>Max)
				{
					Max = fabs(mas[i][j]); I = i; J = j;
				}
			}
		}
		//������ ������������ ����� � �������� ���, ��� �� 
		//������� ������� �������� �� ����� M[k][k]
		if (Max == 0)
		{
			delete[] Change;
		}
		if (I != k)
		{
			double* T = mas[I];
			mas[I] = mas[k];
			mas[k] = T;
			double T2 = f[I];
			f[I] = f[k];
			f[k] = T2;
		}
		if (J != k)
		{
			for (int i = 0; i<n; i++)
			{
				double T = mas[i][J];
				mas[i][J] = mas[i][k];
				mas[i][k] = T;
			}
			int T2 = Change[J];
			Change[J] = Change[k];
			Change[k] = T2;
		}

		//��������� k-�� ����������� �� ����� k+1,...,N-1
		for (int i = k + 1; i<n; i++)
		{
			t = mas[i][k] / mas[k][k];
			tmp = f[i] - t*f[k];
			f[i] = tmp;
			for (int j = 0; j<n; j++)
			{
				tmp = mas[i][j] - t*mas[k][j];
				mas[i][j] = tmp;
			}
		}
	}//������ ���

	 //�������� ���
	tmp = f[n - 1] / mas[n - 1][n - 1];
	Solution[n - 1] = tmp;
	//newX[n-1]=tmp;
	for (int k = n - 2; k >= 0; k--)
	{
		tmp = f[k];
		for (int j = k + 1; j<n; j++)
			tmp -= mas[k][j] * Solution[j];
		tmp /= mas[k][k];
		Solution[k] = tmp;
	}

	//������ �������� ������������ � ������� �������
	for (int i = 0; i<n - 1; i++)
	{
		for (int j = i; j<n; j++)
		{
			if (Change[j] == i)
			{
				double T = Solution[i];
				Solution[i] = Solution[j];
				Solution[j] = T;
				double T2 = Change[i];
				Change[i] = i;
				Change[j] = T2;
				break;
			}
		}
	}

	delete[] Change;
	return true;
}