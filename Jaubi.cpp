#include "stdafx.h"
#include "Jaubi.h"


int Jaubi(double** A, double *f, double *x, int n, double eps) 
{
	int iteration = 0;
	int i, j;
	double norm, dinv;
	double *xp = new double[n];

	for (i = 0; i < n; i++)
	{
		if (!A[i][i]) return -1;
		dinv = 1 / A[i][i];
		for (j = 0; j < i; j++) A[i][j] *= dinv;
		for (j = i + 1; j < n; j++) A[i][j] *= dinv;
		f[i] *= dinv;
	}
	do {
		iteration++;

		for (i = 0; i < n; i++)
		{
			xp[i] = x[i];
		}
		for (i = 0; i < n; i++)
		{
			x[i] = f[i];
			for (j = 0; j < i; j++)			x[i] -= A[i][j] * xp[j];
			for (j = i + 1; j < n; j++)		x[i] -= A[i][j] * xp[j];
		}
		norm = 0.;
		for (i = 0; i < n; i++)
		{
			if ( fabs(x[i] - xp[i]) > norm) 
				norm = fabs(x[i] - xp[i]);
			x[i] = xp[i];
		}
	}	while (norm > eps);
		
	return iteration;


}