// GaussV4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Generate.h"
#include "Jaubi.h"

#define N 100
#define ALPHA 1.
#define BETA 1e+5

using namespace std;

double norm(double *z, int n);
void rcalcul(int n, double **matrix, double*f, double*x, double* r);
void Ax(int size, double **matrix, double *f, double *x);


int main()
{
	int n = N;
	int it = 0; //�������� ������ �����
	double ** matrix = new double*[n];
	for (int i = 0; i < n; i++)
	{
		matrix[i] = new double[n];
	}
	double ** matrix2 = new double*[n];
	double ** matrix_inv = new double*[n];
	for (int i = 0; i < n; i++)
	{
		matrix2[i] = new double[n];
		matrix_inv[i] = new double[n];

	}
	//���������� �������
	double alpha = ALPHA;
	double beta = BETA;
	
	mygen(matrix, matrix_inv, n, alpha, beta, 1, 2, 0, 1); // ������������
		//mygen(matrix, matrix, n, alpha, beta, 1, 2, 1, 1); //������� ������� ���������
		//alpha = alpha/10;
		//mygen(matrix, matrix3, n, alpha, beta, 0, 0, 2, 1); //��������� �������
	

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				matrix2[i][j] = matrix[i][j];
			}
		}
		cout << '\n';

		//������ ������ �������
		double *x = new double[n];
		double *xt = new double[n];
		//xt[0] = 3.; xt[1] = 2.; xt[2] = 1.;
		for (int i = 0; i < n; i++)
		{
			xt[i] = 1;
			
			//��������� ����������� ��� �����
			x[i] = 0.;
		}

		//������� ������ �����
		double *f = new double[n];
		double *f2 = new double[n];
		Ax(n, matrix, xt, f);
		for (int i = 0; i < n; i++)
		{
			f2[i] = f[i];
		}

		it = Jaubi(matrix, f2, x, n, 0.1);
		if (it == 0)
		{
			cout << "Error has ocured" << endl;
		}
		else
		{
			//������������ ������������� 
			cout << "Iterations =  " << it << endl;
			//������
			double *z = new double[n];
			for (int i = 0; i < n; i++) {
				z[i] = x[i] - xt[i];
			}
			/*for (int i = 0; i < n; i++) {
				cout << " Vector Xt =  " << xt[i] << endl;
			}*/
 			//����� ������
			double znorm = norm(z, n);
			cout << " || z || = " << znorm << endl;

			//������������� ������
			double dz = znorm / norm(xt, n);
			cout << " || dz || = " << dz << endl;

			//�������
			double *r = new double[n];
			rcalcul(n, matrix2, f2, x, r);
			cout << "Discrepancy r = (" << r[0] << "; " << r[1] << "; " << r[2] << ")" << endl;

			//����� �������
			double rnorm = norm(r, n);
			cout << " || r || = " << rnorm << endl;

			//������������� ����� �������
			double rho = rnorm / norm(f2, n);
			cout << " || p || = " << rho << endl << endl;
		}
		cin.get();
	
	return 0;
}


double norm(double *z, int n) {
	//������� ����������� �����
	double max = fabs(z[0]);
	for (int i = 1; i < n; i++) {
		if (fabs(z[i]) > max)
			max = fabs(z[i]);
	}

	return max;
}

void rcalcul(int n, double **matrix, double*f, double*x, double* r) {
	//���������� ������� r
	for (int i = 0; i < n; i++) {
		r[i] = -f[i];
		for (int j = 0; j < n; j++) {
			r[i] += matrix[i][j] * x[j];
		}
	}
}


void Ax(int size, double **matrix, double *x, double *f) {
	//��������� ������� �� ������ �
	for (int i = 0; i < size; i++) {
		f[i] = 0.;
		for (int j = 0; j < size; j++) {
			f[i] += x[j] * matrix[i][j];
		}
	}
}